package com.example.demo_mvvm

import androidx.lifecycle.ViewModel
import com.example.demo_mvvm.model.MealsRepository
import com.example.demo_mvvm.model.response.MealCategoriesResponse

class MealCategoriesViewModel(
    private val repository: MealsRepository = MealsRepository(),
) : ViewModel() {

    fun getMeals(successCallBack: (response: MealCategoriesResponse?) -> Unit) {
        repository.getMeals { response ->
            successCallBack(response)
        }
    }
}