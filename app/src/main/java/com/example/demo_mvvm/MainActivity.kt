package com.example.demo_mvvm

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.demo_mvvm.model.response.MealCategoriesResponse
import com.example.demo_mvvm.model.response.MealResponse

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            val viewModel by viewModels<MealCategoriesViewModel>()
            Surface(modifier = Modifier.fillMaxSize(),
                color = MaterialTheme.colors.background) {
                MealsCategoriesScreen()
            }
        }
    }
}

@Composable
fun MealsCategoriesScreen() {
    val viewModel: MealCategoriesViewModel = viewModel()
    val rememberMeals: MutableState<List<MealResponse>> = remember {
        mutableStateOf(emptyList())
    }
    viewModel.getMeals { response: MealCategoriesResponse? ->
        val meals = response?.categories
        rememberMeals.value = meals.orEmpty()
    }
    LazyColumn {
        items(rememberMeals.value) { meal ->
            Text(text = meal.name)
        }
    }
}


@Composable
fun ProfileContent() {

}


@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    MealsCategoriesScreen()
}