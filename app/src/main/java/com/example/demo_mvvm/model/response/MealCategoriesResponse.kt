package com.example.demo_mvvm.model.response

import com.google.gson.annotations.SerializedName

data class MealCategoriesResponse(var categories: List<MealResponse>)

data class MealResponse(
    @SerializedName("idCategory")
    private val id: String,
    @SerializedName("strCategory") val name: String,
    @SerializedName("strCategoryDescription")
    private val description: String,
    @SerializedName("strCategoryThumb")
    private val imageUrl: String,
)