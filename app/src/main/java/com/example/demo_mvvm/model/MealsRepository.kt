package com.example.demo_mvvm.model

import com.example.demo_mvvm.model.api.MealsWebService
import com.example.demo_mvvm.model.response.MealCategoriesResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MealsRepository(private var webService: MealsWebService = MealsWebService()) {
    fun getMeals(successCallback: (response: MealCategoriesResponse?) -> Unit) {
        return webService.getMeals().enqueue(object : Callback<MealCategoriesResponse> {
            override fun onResponse(
                call: Call<MealCategoriesResponse>,
                response: Response<MealCategoriesResponse>,
            ) {
                if (response.isSuccessful)
                    successCallback(response.body())
            }

            override fun onFailure(call: Call<MealCategoriesResponse>?, t: Throwable?) {
                //TODO
            }

        })
    }
}